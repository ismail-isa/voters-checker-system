<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKecamatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kecamatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('kelurahan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kecamatan_id')->unsigned();
            $table->string('name', 100);
            $table->timestamps();

            $table->foreign('kecamatan_id')->references('id')->on('kecamatan')
                ->onUpdate('cascade')->onDelete('cascade');

        });

        Schema::create('tps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelurahan_id')->unsigned();
            $table->string('name', 50);
            $table->timestamps();

            $table->foreign('kelurahan_id')->references('id')->on('kelurahan')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tps');
        Schema::drop('kelurahan');
        Schema::drop('kecamatan');
    }
}
