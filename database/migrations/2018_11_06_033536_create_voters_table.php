<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik', 100)->unique();
            $table->string('name', 100);
            $table->string('address', 100);
            $table->enum('gender', ['Male', 'Female']);
            $table->integer('tps_id')->unsigned();
            $table->timestamps();

            $table->foreign('tps_id')->references('id')->on('tps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voters');
    }
}
