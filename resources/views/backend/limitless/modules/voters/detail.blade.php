@extends('backend.limitless.inc.app')
@section('title', 'Daftar Pemilih Tetap - Sistem Rekapitulasi Suara')

@section('content')
	@include('backend.limitless.inc.navbar')
	<!-- Page content -->
	<div class="page-content">
		@include('backend.limitless.inc.sidebar')
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Daftar Pemilih Tetap</span> - {{$d->name}}</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
							<a href="{{ route('voters') }}" class="breadcrumb-item"> Daftar Pemilih Tetap</a>
							<span class="breadcrumb-item active">{{$d->name}}</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>
			</div>
			<!-- /page header -->

			<!-- Content area -->
			<div class="content">
				<!-- Basic layout-->
				<div class="card">
					<div class="card-header bg-light header-elements-inline">
						<h5 class="card-title">Detail Data DPT</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>
					<div class="card-body">
						<div class="row">
							<dl class="mb-0 col-md-4">
								<dt>Nama Lengkap</dt>
								<dd>{{$d->name}}</dd>
								<dt>NIK</dt>
								<dd>{{$d->nik}}</dd>
								<dt>Alamat</dt>
								<dd>{{ $d->address or '-' }}</dd>
								<dt>Jenis Kelamin</dt>
								<dd>{{$d->gender}}</dd>
							</dl>
							<dl class="mb-0 col-md-4">
								<dt>Kecamatan</dt>
								<dd>{{$d->kecamatan}}</dd>
								<dt>Desa/Kelurahan</dt>
								<dd>{{$d->kelurahan}}</dd>
								<dt>TPS</dt>
								<dd>{{$d->tps}}</dd>
								<dt>Dibuat Oleh</dt>
								<dd>{{$d->creator}}</dd>
							</dl>
							<div class="col-md-4">
								@if (!empty($d->photo)) 
									<img src="{{url('id_card/', $d->photo)}}" class="img-fluid rounded">
								@else
								@endif
							</div>
						</div>
					</div>
					<div class="card-footer bg-white d-flex justify-content-between align-items-center">
						<a href="{{ route('voters') }}" class="btn btn-warning">Kembali</a>
					</div>
				</div>
				<!-- /basic layout -->
			</div>
			<!-- /content -->

			@include('backend.limitless.inc.footer')
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
@endsection

@section('singlejs')
@endsection