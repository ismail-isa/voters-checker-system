@extends('backend.limitless.inc.app')
@section('title', 'Daftar Pemilih Tetap - Sistem Rekapitulasi Suara')

@section('content')
	@include('backend.limitless.inc.navbar')
	<!-- Page content -->
	<div class="page-content">
		@include('backend.limitless.inc.sidebar')
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Daftar Pemilih Tetap</span> - Data Baru</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
							<a href="{{ route('voters') }}" class="breadcrumb-item"> Daftar Pemilih Tetap</a>
							<span class="breadcrumb-item active">Data Baru</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>
			</div>
			<!-- /page header -->

			<!-- Content area -->
			<div class="content">
				<!-- Basic layout-->
				<div class="card">
					<div class="card-header bg-light header-elements-inline">
						<h5 class="card-title">Form Daftar Pemilih Tetap</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>
					<form action="{{ route('votersCreateSubmit') }}" method="post" enctype="multipart/form-data">
						<div class="card-body">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-warning border-0 alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
									<ul>
									@foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        	</ul>
								</div>
							@endif
							<div class="form-group">
								<label>NIK<span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="nik" value="{{old('nik')}}"  required>
							</div>
							<div class="form-group">
								<label>Nama<span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="name" value="{{old('name')}}"  required>
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<input type="text" class="form-control" name="address" value="{{old('address')}}">
							</div>
							<div class="form-group">
								<label class="d-block">Jenis Kelamin</label>

								<div class="form-check form-check-inline">
									<label class="form-check-label">
										<input type="radio" class="form-input-styled" name="gender" checked data-fouc value="L">
										Laki-laki
									</label>
								</div>

								<div class="form-check form-check-inline">
									<label class="form-check-label">
										<input type="radio" class="form-input-styled" name="gender" data-fouc value="P">
										Perempuan
									</label>
								</div>
							</div>
							<div class="form-group">
								<label>Kecamatan<span class="text-danger">*</span></label>
								<select class="form-control" name="kecamatan" id="kecamatan" required>
	                                <option value="" selected>Pilih Kecamatan</option>
	                                @foreach($kecamatan as $k)
                                        <option value="{{ $k->id }}">{{ $k->name }}</option>
                                    @endforeach
	                            </select>
							</div>
							<div class="form-group">
								<label>Desa/Kelurahan<span class="text-danger">*</span></label>
								<select class="form-control" name="kelurahan" id="kelurahan" required>
	                                <option value="" selected>Pilih Kecamatan Dulu</option>
	                            </select>
							</div>
							<div class="form-group">
								<label>TPS<span class="text-danger">*</span></label>
								<select class="form-control" name="tps" id="tps" required>
	                                <option value="" selected>Pilih Desa Dulu</option>
	                            </select>
							</div>
							<div class="form-group">
								<label>Foto KTP</label>
								<input type="file" name="identity_photo" class="form-control-uniform" data-fouc>
							</div>
						</div>

						<div class="card-footer bg-white d-flex justify-content-between align-items-center">
							<button type="submit" class="btn btn-success">Simpan</button>
							<a href="{{ route('voters') }}" class="btn btn-light">Kembali</a>
						</div>
					</form>
				</div>
				<!-- /basic layout -->
			</div>
			<!-- /content -->

			@include('backend.limitless.inc.footer')
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
@endsection

@section('singlejs')
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/notifications/noty.min.js') }}"></script>
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#kecamatan').on('change', function() {
        var kecamatan = $(this).val();
        $.ajax({
            url: '<?php echo url('ajax/kelurahan') ?>/'+kecamatan,
            type: "GET",
            dataType: "json",
            success:function(data) {
                $('#kelurahan').empty();
                $('#tps').empty();
                $('#kelurahan').append('<option value="">Select Village</option>');
                $.each(data, function(key, value) {
                    $('#kelurahan').append('<option value="'+ key +'">'+ value +'</option>');
                });
                $('#tps').append('<option value=""> Select Village First </option>');
            }
        });
    });
    $('#kelurahan').on('change', function() {
        var kelurahan = $(this).val();
        $.ajax({
            url: '<?php echo url('ajax/tps') ?>/'+kelurahan,
            type: "GET",
            dataType: "json",
            success:function(data) {
                $('#tps').empty();
                $('#tps').append('<option value="">Select TPS</option>');
                $.each(data, function(key, value) {
                    $('#tps').append('<option value="'+ key +'">'+ value +'</option>');
                });
            }
        });
    });
});

var Plugins = function() {
	var _componentNoty = function() {
        if (typeof Noty == 'undefined') {
            console.warn('Warning - noty.min.js is not loaded.');
            return;
        }

        // Override Noty defaults
        Noty.overrideDefaults({
            theme: 'limitless',
            layout: 'topRight',
            type: 'alert',
            timeout: 3000
        });
        @if(Session::has('message'))
	        new Noty({
	            text: '{{ Session::get('message') }}',
	            type: 'success'
	        }).show();
	    @endif
    };

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-pink-400'
        });

        $('.form-control-uniform').uniform();
    };

    // Return objects assigned to module
    return {
        init: function() {
            _componentUniform();
            _componentNoty();
        }
    }
}();

// Initialize module
document.addEventListener('DOMContentLoaded', function() {
    Plugins.init();
});
</script>
@endsection