@extends('backend.limitless.inc.app')
@section('title', 'TPS - Sistem Rekapitulasi Suara')

@section('content')
	@include('backend.limitless.inc.navbar')
	<!-- Page content -->
	<div class="page-content">
		@include('backend.limitless.inc.sidebar')
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">TPS</span> - Edit Data</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
							<a href="{{ route('tps', ['kecamatan' => $kecamatan,'kelurahan' => $kelurahan]) }}" class="breadcrumb-item"> TPS</a>
							<span class="breadcrumb-item active">Edit Data</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>
			</div>
			<!-- /page header -->

			<!-- Content area -->
			<div class="content">
				<!-- Basic layout-->
				<div class="card">
					<div class="card-header bg-light header-elements-inline">
						<h5 class="card-title">Form TPS</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>
					<form action="{{ route('tpsUpdateSubmit') }}" method="post">
						<div class="card-body">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-warning border-0 alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
									<ul>
									@foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        	</ul>
								</div>
							@endif
							<input type="hidden" name="id" value="{{$d->id}}">
							<input type="hidden" name="kelurahan_id" value="{{$d->kelurahan_id}}">
							<div class="form-group">
								<label>Nama<span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="name" value="{{$d->name}}"  required>
							</div>
						</div>

						<div class="card-footer bg-white d-flex justify-content-between align-items-center">
							<button type="submit" class="btn btn-success">Simpan</button>
							<a href="{{ route('tps', ['kecamatan' => $kecamatan,'kelurahan' => $kelurahan]) }}" class="btn btn-light">Kembali</a>
						</div>
					</form>
				</div>
				<!-- /basic layout -->
			</div>
			<!-- /content -->

			@include('backend.limitless.inc.footer')
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
@endsection

@section('singlejs')
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/notifications/noty.min.js') }}"></script>
<script type="text/javascript">
var Plugins = function() {
	var _componentNoty = function() {
        if (typeof Noty == 'undefined') {
            console.warn('Warning - noty.min.js is not loaded.');
            return;
        }

        // Override Noty defaults
        Noty.overrideDefaults({
            theme: 'limitless',
            layout: 'topRight',
            type: 'alert',
            timeout: 3000
        });
        @if(Session::has('message'))
	        new Noty({
	            text: '{{ Session::get('message') }}',
	            type: 'success'
	        }).show();
	    @endif
    };

    // Return objects assigned to module
    return {
        init: function() {
            _componentNoty();
        }
    }
}();

// Initialize module
document.addEventListener('DOMContentLoaded', function() {
    Plugins.init();
});
</script>
@endsection