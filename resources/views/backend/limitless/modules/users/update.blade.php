@extends('backend.limitless.inc.app')
@section('title', 'User - Sistem Rekapitulasi Suara')

@section('content')
	@include('backend.limitless.inc.navbar')
	<!-- Page content -->
	<div class="page-content">
		@include('backend.limitless.inc.sidebar')
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">User</span> - Edit Data</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
							<a href="{{ route('users') }}" class="breadcrumb-item"> User</a>
							<span class="breadcrumb-item active">Edit Data</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>
			</div>
			<!-- /page header -->

			<!-- Content area -->
			<div class="content">
				<!-- Basic layout-->
				<div class="card">
					<div class="card-header bg-light header-elements-inline">
						<h5 class="card-title">Form User</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>
					<form action="{{ route('usersUpdateSubmit') }}" method="post">
						<div class="card-body">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-warning border-0 alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
									<ul>
									@foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        	</ul>
								</div>
							@endif
							<input type="hidden" name="id" value="{{$d->id}}">
							<div class="form-group">
								<label>Nama<span class="text-danger">*</span></label>
								<input type="text" class="form-control" name="name" value="{{$d->name}}"  required>
							</div>
							<div class="form-group">
								<label>Email<span class="text-danger">*</span></label>
								<input type="email" class="form-control" name="email" value="{{$d->email}}" required>
							</div>
							<div class="form-group">
								<label>Telpon</label>
								<input type="text" class="form-control" name="telpon" value="{{$d->telpon}}">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="text" class="form-control" name="password" value="{{old('password')}}">
								<span class="form-text text-muted">biarkan kosong jika tidak diganti.</span>
							</div>
							<div class="form-group">
								<label>Konfirmasi Password</label>
								<input type="text" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
								<span class="form-text text-muted">biarkan kosong jika tidak diganti.</span>
							</div>
							<div class="form-group">
								<label>Roles<span class="text-danger">*</span></label>
								<select class="form-control" name="role" required>
	                                @foreach($roles as $r)
	                                	@if($r->id == $d->role_id)
                                        	<option value="{{ $r->id }}" selected>{{ $r->display_name }}</option>
                                        @else
                                            <option value="{{ $r->id }}">{{ $r->display_name }}</option>
                                        @endif
                                    @endforeach
	                            </select>
							</div>
							<div class="form-group">
								<label>Kecamatan<span class="text-danger">*</span></label>
								<select class="form-control" name="kecamatan" id="kecamatan" required>
	                                @foreach($kecamatan as $k)
	                                	@if($k->id == $d->kecamatan)
                                        	<option value="{{ $k->id }}" selected>{{ $k->name }}</option>
                                        @else
                                            <option value="{{ $k->id }}">{{ $k->name }}</option>
                                        @endif
                                    @endforeach
	                            </select>
							</div>
							<div class="form-group">
								<label>Desa/Kelurahan<span class="text-danger">*</span></label>
								<select class="form-control" name="kelurahan" id="kelurahan" required>
	                                @foreach($kelurahan as $l)
	                                	@if($l->id == $d->kelurahan)
                                        	<option value="{{ $l->id }}" selected>{{ $l->name }}</option>
                                        @else
                                            <option value="{{ $l->id }}">{{ $l->name }}</option>
                                        @endif
                                    @endforeach
	                            </select>
							</div>
						</div>

						<div class="card-footer bg-white d-flex justify-content-between align-items-center">
							<button type="submit" class="btn btn-success">Simpan</button>
							<a href="{{ route('users') }}" class="btn btn-light">Kembali</a>
						</div>
					</form>
				</div>
				<!-- /basic layout -->
			</div>
			<!-- /content -->

			@include('backend.limitless.inc.footer')
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
@endsection

@section('singlejs')
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/notifications/noty.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#kecamatan').on('change', function() {
        var kecamatan = $(this).val();
        $.ajax({
            url: '<?php echo url('ajax/kelurahan') ?>/'+kecamatan,
            type: "GET",
            dataType: "json",
            success:function(data) {
                $('#kelurahan').empty();
                $('#kelurahan').append('<option value="">Select Village</option>');
                $.each(data, function(key, value) {
                    $('#kelurahan').append('<option value="'+ key +'">'+ value +'</option>');
                });
            }
        });
    });
});
var Plugins = function() {
	var _componentNoty = function() {
        if (typeof Noty == 'undefined') {
            console.warn('Warning - noty.min.js is not loaded.');
            return;
        }

        // Override Noty defaults
        Noty.overrideDefaults({
            theme: 'limitless',
            layout: 'topRight',
            type: 'alert',
            timeout: 3000
        });
        @if(Session::has('message'))
	        new Noty({
	            text: '{{ Session::get('message') }}',
	            type: 'success'
	        }).show();
	    @endif
    };

    // Return objects assigned to module
    return {
        init: function() {
            _componentNoty();
        }
    }
}();

// Initialize module
document.addEventListener('DOMContentLoaded', function() {
    Plugins.init();
});
</script>
@endsection