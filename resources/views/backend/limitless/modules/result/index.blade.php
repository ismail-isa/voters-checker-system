@extends('backend.limitless.inc.app')
@section('title', 'Hasil Rakapitulasi Pemilihan - Sistem Rekapitulasi Suara')

@section('content')
	@include('backend.limitless.inc.navbar')
	<!-- Page content -->
	<div class="page-content">
		@include('backend.limitless.inc.sidebar')
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Hasil Rakapitulasi Pemilihan</span> - Data</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
							<span class="breadcrumb-item active">Hasil Rakapitulasi Pemilihan</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>
				</div>
			</div>
			<!-- /page header -->
			
			<!-- Content area -->
			<div class="content">
				<!-- Basic datatable -->
				<div class="card">
					<div class="card-header bg-transparent header-elements-inline">
						<h5 class="card-title">Data Hasil Rakapitulasi Pemilihan</h5>
						<div class="header-elements">
							@permission('result-create')
								<a href="{{ route('resultCreate') }}" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-plus3"></i></b> Buat Baru</a>
	                		@endpermission
	                	</div>
					</div>

					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Kecamatan</th>
								<th>Desa/Kelurahan</th>
								<th>TPS</th>
								<th>Jumlah Suara</th>
								<th>Tgl Dibuat</th>
								<th>Dibuat Oleh</th>
								<th class="text-center">Menu</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $d)
								<tr>
									<td>{{$d->kecamatan}}</td>
									<td>{{$d->kelurahan}}</td>
									<td>{{$d->tps_id}}</td>
									<td>{{$d->amount}}</td>
									<td>{{ date('j/n/Y', strtotime($d->created_at)) }}</td>
									<td>{{$d->creator}}</td>
									<td class="text-center">
										<div class="list-icons">
											<div class="dropdown">
												<a href="#" class="list-icons-item" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right">
													@permission('result-update')
														<a href="{{ route('resultUpdate', ['id' => $d->id]) }}" class="dropdown-item"><i class="icon-compose"></i> Edit</a>
													@endpermission
													@permission('result-delete')
														<a href="#" data-action-target="{{ route('resultDelete', ['id' => $d->id]) }}" class="dropdown-item" data-toggle="modal" data-target="#modal-delete"><i class="icon-trash"></i> Hapus</a>
													@endpermission
												</div>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /basic datatable -->
			</div>
			<!-- /content -->

			@include('backend.limitless.inc.footer')
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
@endsection

@section('singlejs')
<!-- Danger modal -->
<div id="modal-delete" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<h6 class="modal-title">Konfirmasi Hapus Data</h6>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">
				Apakah anda yakin akan menghapus data ini? 
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<a id="delete" href="" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>
<!-- /default modal -->
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/notifications/noty.min.js') }}"></script>
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript">
$( document ).ready(function() {
    $('#modal-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var actionTarget = button.data('action-target');
        var modal = $(this);
        modal.find('#delete').attr('href', actionTarget);
    });
});
var Plugins = function () {
	var _componentNoty = function() {
        if (typeof Noty == 'undefined') {
            console.warn('Warning - noty.min.js is not loaded.');
            return;
        }

        // Override Noty defaults
        Noty.overrideDefaults({
            theme: 'limitless',
            layout: 'topRight',
            type: 'alert',
            timeout: 3000
        });
        @if(Session::has('message'))
	        new Noty({
	            text: '{{ Session::get('message') }}',
	            type: 'success'
	        }).show();
	    @endif
    };
    // Basic Datatable
    var _componentDatatableBasic = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }
        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
        	order: [],
            autoWidth: false,
            columnDefs: [{ 
                orderable: false,
                width: 100
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Ketik disini...',
                lengthMenu: '<span>Tampil:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });
        // Basic datatable
        $('.datatable-basic').DataTable();
        // Resize scrollable table when sidebar width changes
        $('.sidebar-control').on('click', function() {
            table.columns.adjust().draw();
        });
    };
    // Select2 for length menu styling
    var _componentSelect2 = function() {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }
        // Initialize
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    };
    // Return objects assigned to module
    return {
        init: function() {
            _componentDatatableBasic();
            _componentSelect2();
            _componentNoty();
        }
    }
}();

// Initialize module
document.addEventListener('DOMContentLoaded', function() {
    Plugins.init();
});	
</script>
@endsection