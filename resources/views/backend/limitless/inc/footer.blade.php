<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
	<div class="text-center d-lg-none w-100">
		<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
			<i class="icon-unfold mr-2"></i>
			Footer
		</button>
	</div>

	<div class="navbar-collapse collapse" id="navbar-footer">
		<span class="navbar-text">
			&copy; 2015 - 2019. ISA CMS developed by <a href="http://idesolusiasia" target="_blank">PT. Ide Solusi Asia - Web Development Team</a>
		</span>
	</div>
</div>
<!-- /footer -->