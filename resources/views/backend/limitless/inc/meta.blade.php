<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title></title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('backend/limitless/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('backend/limitless/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('backend/limitless/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('backend/limitless/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('backend/limitless/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('backend/limitless/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->