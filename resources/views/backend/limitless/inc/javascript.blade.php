<!-- Core JS files -->
<script src="{{ URL::asset('backend/limitless/assets/js/main/jquery.min.js') }}"></script>
<script src="{{ URL::asset('backend/limitless/assets/js/main/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::asset('backend/limitless/assets/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="{{ URL::asset('backend/limitless/assets/js/app.js') }}"></script>
<!-- /theme JS files -->