<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

	<!-- Sidebar mobile toggler -->
	<div class="sidebar-mobile-toggler text-center">
		<a href="#" class="sidebar-mobile-main-toggle">
			<i class="icon-arrow-left8"></i>
		</a>
		Menu
		<a href="#" class="sidebar-mobile-expand">
			<i class="icon-screen-full"></i>
			<i class="icon-screen-normal"></i>
		</a>
	</div>
	<!-- /sidebar mobile toggler -->


	<!-- Sidebar content -->
	<div class="sidebar-content">

		<!-- Main navigation -->
		<div class="card card-sidebar-mobile">
			<ul class="nav nav-sidebar" data-nav-type="accordion">

				<!-- Main -->
				<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Menu Utama</div> <i class="icon-menu" title="Main"></i></li>
				<li class="nav-item">
					<a href="{{ route('dashboard') }}" class="nav-link {{ activeMenu('dashboard') }}">
						<i class="icon-home4"></i>
						<span>Dashboard</span>
					</a>
				</li>
				@permission('voters')
				<li class="nav-item">
					<a href="{{ route('voters') }}" class="nav-link {{ activeMenu('voters') }}">
						<i class="icon-vcard"></i>
						<span>Daftar Pemilih Tetap</span>
					</a>
				</li>
				@endpermission
				@permission('result')
				<li class="nav-item">
					<a href="{{ route('result') }}" class="nav-link {{ activeMenu('result') }}">
						<i class="icon-chart"></i>
						<span>Hasil Realcount</span>
					</a>
				</li>
				@endpermission
				@permission('districts')
				<li class="nav-item">
					<a href="{{ route('districts') }}" class="nav-link {{ activeMenu('districts,villages,tps') }}">
						<i class="icon-wrench3"></i>
						<span>Konfigurasi</span>
					</a>
				</li>
				@endpermission
				@role('admin')
				<li class="nav-item">
					<a href="{{ route('users') }}" class="nav-link {{ activeMenu('users') }}">
						<i class="icon-users"></i>
						<span>Manajemen User</span>
					</a>
				</li>
				@endrole
			</ul>
		</div>
		<!-- /main navigation -->
	</div>
	<!-- /sidebar content -->
</div>
<!-- /main sidebar -->