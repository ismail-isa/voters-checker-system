<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Login - Sistem Rekapitulasi Suara</title>
	@include('backend.limitless.inc.meta')
	<style>
        .login-style-container{
            min-height: 100vh;
        }

        .el-middle{
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .bg-login-style{
            background-image: url("{{ URL::asset('backend/limitless/assets/images/bg-1.jpg') }}");
            background-repeat: no-repeat;
            background-position: right top;
            background-color: #148c01;
            min-height: 200px;
        }
    </style>
</head>
<body>
	<!-- Page content -->
	<div class="page-content">
		<!-- Main content -->
		<div class="content-wrapper">
			<div class="row m-0 login-style-container el-middle ">
                <div class="col-sm-7 bg-white text-black">
                    <div class="row">
                        <div class="col-sm-6 bg-login-style">
                        </div>
                        <div class="col-sm-6">
                            <div class="card-body p-4">
                                <div class="media">
                                    <div class="media-body">
                                         <h2 class="font-weight-semibold text-grey-500 mb-0">Login</h2>
                                        <div class="text-grey-300 mb-3">
                                            Silahkan masukkan email dan password.
                                        </div>
                                    </div>
                                </div>
                                <form action="{{ route('loginProcess') }}" method="post">
									{{ csrf_field() }}
									@if (count($errors) > 0)
									<div class="alert alert-warning alert-styled-left alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
										@foreach ($errors->all() as $error)
											{{ $error }}
										@endforeach
								    </div>
								    @endif
	                                <div class="form-group">
	                                    <div class="input-group">
	                                        <span class="input-group-prepend">
	                                            <span class="input-group-text">
	                                                <i class="icon-user-tie"></i>
	                                            </span>
	                                        </span>
	                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
	                                    </div>
	                                </div>
	                                 <div class="form-group">
	                                    <div class="input-group">
	                                        <span class="input-group-prepend">
	                                            <span class="input-group-text">
	                                                <i class="icon-lock2"></i>
	                                            </span>
	                                        </span>
	                                        <input type="password" name="password" class="form-control" placeholder="Password" required>
	                                    </div>
	                                </div>
	                                <div class="row">
	                                    <div class="col-sm-12 text-center">
	                                        <button type="submit" class="btn bg-success btn-labeled btn-labeled-left rounded-round legitRipple">
	                                            <b><i class="icon-arrow-right8"></i></b> Login
	                                        </button>
	                                    </div>
	                                </div>
	                            </form>
								<!-- /login form -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- /main content -->
	</div>
	<!-- /page content -->
	@include('backend.limitless.inc.javascript')
	@yield('singlejs')
</body>
</html>