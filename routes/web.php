<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BackEndController@login')->name('login');
Route::post('/login-process', 'BackEndController@loginProcess')->name('loginProcess');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'BackEndController@dashboard')->name('dashboard');
	Route::get('/logout', 'BackEndController@logout')->name('logout');

    Route::group(['prefix' => 'users'], function() {
    	Route::get('/', ['middleware' => ['permission:users'], 'uses' => 'BackEndController@users'])->name('users');
	    Route::get('/create', ['middleware' => ['permission:users-create'], 'uses' => 'BackEndController@usersCreate'])->name('usersCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:users-create-submit'], 'uses' => 'BackEndController@usersCreateSubmit'])->name('usersCreateSubmit');
		Route::get('/update/{id}', ['middleware' => ['permission:users-update'], 'uses' => 'BackEndController@usersUpdate'])->name('usersUpdate');
		Route::post('/update-submit', ['middleware' => ['permission:users-update-submit'], 'uses' => 'BackEndController@usersUpdateSubmit'])->name('usersUpdateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:users-delete'], 'uses' => 'BackEndController@usersDelete'])->name('usersDelete');
	});

	Route::group(['prefix' => 'voters'], function() {
	    Route::get('/', ['middleware' => ['permission:voters'], 'uses' => 'BackEndController@voters'])->name('voters');
	    Route::get('/create', ['middleware' => ['permission:voters-create'], 'uses' => 'BackEndController@votersCreate'])->name('votersCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:voters-create-submit'], 'uses' => 'BackEndController@votersCreateSubmit'])->name('votersCreateSubmit');
		Route::get('/detail/{id}', ['middleware' => ['permission:voters'], 'uses' => 'BackEndController@votersDetail'])->name('votersDetail');
		Route::get('/update/{id}', ['middleware' => ['permission:voters-update'], 'uses' => 'BackEndController@votersUpdate'])->name('votersUpdate');
		Route::post('/update-submit', ['middleware' => ['permission:voters-update-submit'], 'uses' => 'BackEndController@votersUpdateSubmit'])->name('votersUpdateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:voters-delete'], 'uses' => 'BackEndController@votersDelete'])->name('votersDelete');
	});

	Route::group(['prefix' => 'districts'], function() {
	    Route::get('/', ['middleware' => ['permission:districts'], 'uses' => 'BackEndController@districts'])->name('districts');
	    Route::get('/create', ['middleware' => ['permission:districts-create'], 'uses' => 'BackEndController@districtsCreate'])->name('districtsCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:districts-create-submit'], 'uses' => 'BackEndController@districtsCreateSubmit'])->name('districtsCreateSubmit');
		Route::get('/update/{id}', ['middleware' => ['permission:districts-update'], 'uses' => 'BackEndController@districtsUpdate'])->name('districtsUpdate');
		Route::post('/update-submit', ['middleware' => ['permission:districts-update-submit'], 'uses' => 'BackEndController@districtsUpdateSubmit'])->name('districtsUpdateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:districts-delete'], 'uses' => 'BackEndController@districtsDelete'])->name('districtsDelete');
	});

	Route::group(['prefix' => 'villages'], function() {
	    Route::get('/{id}', ['middleware' => ['permission:villages'], 'uses' => 'BackEndController@villages'])->where('id', '[0-9]+')->name('villages');
	    Route::get('/create/{id}', ['middleware' => ['permission:villages-create'], 'uses' => 'BackEndController@villagesCreate'])->name('villagesCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:villages-create-submit'], 'uses' => 'BackEndController@villagesCreateSubmit'])->name('villagesCreateSubmit');
		Route::get('/update/{kecamatan}/{id}', ['middleware' => ['permission:villages-update'], 'uses' => 'BackEndController@villagesUpdate'])->name('villagesUpdate');
		Route::post('/update-submit', ['middleware' => ['permission:villages-update-submit'], 'uses' => 'BackEndController@villagesUpdateSubmit'])->name('villagesUpdateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:villages-delete'], 'uses' => 'BackEndController@villagesDelete'])->name('villagesDelete');
	});

	Route::group(['prefix' => 'tps'], function() {
	    Route::get('/{kecamatan}/{kelurahan}', ['middleware' => ['permission:tps'], 'uses' => 'BackEndController@tps'])->where('kecamatan', '[0-9]+','kelurahan', '[0-9]+')->name('tps');
	    Route::get('/create/{kecamatan}/{kelurahan}', ['middleware' => ['permission:tps-create'], 'uses' => 'BackEndController@tpsCreate'])->name('tpsCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:tps-create-submit'], 'uses' => 'BackEndController@tpsCreateSubmit'])->name('tpsCreateSubmit');
		Route::get('/update/{kecamatan}/{kelurahan}/{id}', ['middleware' => ['permission:tps-update'], 'uses' => 'BackEndController@tpsUpdate'])->name('tpsUpdate');
		Route::post('/update-submit', ['middleware' => ['permission:tps-update-submit'], 'uses' => 'BackEndController@tpsUpdateSubmit'])->name('tpsUpdateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:tps-delete'], 'uses' => 'BackEndController@tpsDelete'])->name('tpsDelete');
	});

	Route::group(['prefix' => 'result'], function() {
    	Route::get('/', ['middleware' => ['permission:result'], 'uses' => 'BackEndController@result'])->name('result');
	    Route::get('/create', ['middleware' => ['permission:result-create'], 'uses' => 'BackEndController@resultCreate'])->name('resultCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:result-create-submit'], 'uses' => 'BackEndController@resultCreateSubmit'])->name('resultCreateSubmit');
		Route::get('/update/{id}', ['middleware' => ['permission:result-update'], 'uses' => 'BackEndController@resultUpdate'])->name('resultUpdate');
		Route::post('/update-submit', ['middleware' => ['permission:result-update-submit'], 'uses' => 'BackEndController@resultUpdateSubmit'])->name('resultUpdateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:result-delete'], 'uses' => 'BackEndController@resultDelete'])->name('resultDelete');
	});

	Route::group(['prefix' => 'supporter'], function() {
	    Route::get('/create', ['middleware' => ['permission:supporter-create'], 'uses' => 'BackEndController@supporterCreate'])->name('supporterCreate');
	    Route::post('/create-submit', ['middleware' => ['permission:supporter-create-submit'], 'uses' => 'BackEndController@supporterCreateSubmit'])->name('supporterCreateSubmit');
		Route::get('/delete/{id}', ['middleware' => ['permission:supporter-delete'], 'uses' => 'BackEndController@supporterDelete'])->name('supporterDelete');
	});
});

Route::group(['prefix' => 'ajax'], function () {
	Route::get('kelurahan/{id}','AjaxController@kelurahan');
	Route::get('tps/{id}','AjaxController@tps');
});
