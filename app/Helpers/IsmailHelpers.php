<?php

/**
 *
 * Set active css class if the specific URI is current URI
 *
 * @param string $path       A specific URI
 * @param string $class_name Css class name, optional
 * @return string            Css class name if it's current URI,
 *                           otherwise - empty string
 */
function activeMenu(string $path, string $class_name = "active")
{
    $data    = explode(",", $path);
  	return ( in_array(Request::segment(1),$data) ) ? $class_name : "";
}