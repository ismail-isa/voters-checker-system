<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Entrust;
use Session;
use Validator;
use DB;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class BackEndController extends Controller
{
	/*Begin Auth Process*/
    public function login(){
    	return view('backend.limitless.auth.login');
    }
	public function loginProcess(Request $request){
		$validator = Validator::make($request->all(), [
			'email' => 'required|exists:users,email',
			'password' => 'required',
		]);
		
		if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        else{
			if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
				return redirect()->intended('dashboard');
			}
			else{
				return back()->withErrors(['Email and Password not match']);
			}
				
		}
    }
    public function logout(){
    	auth()->logout();
		return redirect()->route('login');
    }
    /*end Auth*/

	
	/*Begin Dashboard Module*/
	public function dashboard(){
        if(Entrust::hasRole('admin')){
            $data = DB::table('kecamatan')
                ->leftJoin('kelurahan', 'kelurahan.kecamatan_id', '=', 'kecamatan.id')
                ->leftJoin('tps', 'tps.kelurahan_id', '=', 'kelurahan.id')
                ->leftJoin('voters', 'voters.tps_id', '=', 'tps.id')
                ->select(DB::raw('count(voters.id) as user_count'))
                ->groupBy('kecamatan.id')
                ->orderBy('kecamatan.name', 'asc')
                ->pluck('user_count');
            return view('backend.limitless.modules.dashboard.index-admin')->with('data', $data);
        }else{
            $data = DB::table('supporter')
                ->leftJoin('voters', 'voters.id', '=', 'supporter.id')
                ->leftJoin('tps', 'tps.id', '=', 'voters.tps_id')
                ->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
                ->leftJoin('users', 'users.id', '=', 'supporter.created_by')
                ->select('voters.*', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan', 'users.name AS creator')
                ->where('supporter.created_by', Auth::id())
                ->latest()
                ->get();
            return view('backend.limitless.modules.dashboard.index-coordinator')->with('data', $data);
        }
    }
    public function supporterCreate(){
        $voters = DB::table('voters')->orderBy('nik', 'asc')->get();
        return view('backend.limitless.modules.dashboard.create')->with('voters', $voters);
    }
    public function supporterCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
            'dpt' => 'required|unique:supporter,id',
        ]);
        
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        else{
            DB::table('supporter')->insert([
                'id' => $request->dpt,
                'created_by' => Auth::id(),
                'created_at' => Carbon::now()
            ]);
            Session::flash('message', 'Data telah berhasil dibuat.');
            return back();  
        }
    }
    public function supporterDelete($id){
        DB::table('supporter')->where('id', $id)->delete();
        Session::flash('message', 'Data telah berhasil dihapus.');
        return back();
    }
    /*End Dashboard*/


    /*Begin Voters Module*/
    public function voters(){
        if(Entrust::hasRole('admin')){
            $data = DB::table('voters')
                ->leftJoin('tps', 'tps.id', '=', 'tps_id')
                ->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
                ->leftJoin('users', 'users.id', '=', 'created_by')
                ->select('voters.*', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan', 'tps.name AS tps', 'users.name AS creator')
                ->latest()
                ->get();
        }
        elseif (Entrust::hasRole('coordinator')) {
            $data = DB::table('voters')
                ->leftJoin('tps', 'tps.id', '=', 'tps_id')
                ->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
                ->leftJoin('users', 'users.id', '=', 'created_by')
                ->select('voters.*', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan', 'tps.name AS tps', 'users.name AS creator')
                ->where('created_by', Auth::id())
                ->latest()
                ->get();
        }
    	return view('backend.limitless.modules.voters.index')->with('data', $data);
    }
    public function votersDetail($id){
    	$data = DB::table('voters')
    		->leftJoin('tps', 'tps.id', '=', 'tps_id')
    		->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
    		->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
    		->leftJoin('users', 'users.id', '=', 'created_by')
    		->select('voters.*', 'tps.name AS tps', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan', 'users.name AS creator')
    		->where('voters.id', $id)
    		->first();
    	return view('backend.limitless.modules.voters.detail')->with('d', $data);
    }
    public function votersCreate(){
    	$kecamatan = DB::table('kecamatan')->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.voters.create')->with('kecamatan', $kecamatan);
    }
    public function votersCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'nik' => 'required|min:10|unique:voters,nik',
			'name' => 'required|max:50|min:2',
			'tps' => 'required',
			'identity_photo' => 'image',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
        	if ($request->hasFile('identity_photo')) {
	        	$file = $request->identity_photo;
	        	$extension = $file->getClientOriginalExtension();
	        	$filename  = $request->nik . '.' . $extension;
	            $path = 'id_card/'. $filename;
	            Image::make($file->getRealPath())->widen(800)->save($path);
	        }else{
	        	$filename  = "";
	        }
			DB::table('voters')->insert([
				'nik' => $request->nik, 
				'name' => $request->name, 
				'address' => $request->address, 
				'gender' => $request->gender,
				'tps_id' => $request->tps, 
				'photo' => $filename,
				'created_by' => Auth::id(),
				'created_at' => Carbon::now()
    		]);
			Session::flash('message', 'Data telah berhasil dibuat.');
			return back();	
		}
    }
    public function votersUpdate($id){
    	$data = DB::table('voters')
    		->leftJoin('tps', 'tps.id', '=', 'tps_id')
    		->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
    		->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
    		->select('voters.*', 'kelurahan.id AS kelurahan', 'kecamatan.id AS kecamatan')
    		->where('voters.id', $id)
    		->first();
    	$kecamatan = DB::table('kecamatan')->orderBy('name', 'asc')->get();
    	$kelurahan = DB::table('kelurahan')->where('kecamatan_id', $data->kecamatan)->orderBy('name', 'asc')->get();
    	$tps = DB::table('tps')->where('kelurahan_id', $data->kelurahan)->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.voters.update')
    		->with('kecamatan', $kecamatan)
    		->with('kelurahan', $kelurahan)
    		->with('tps', $tps)
    		->with('d', $data);
    }
    public function votersUpdateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'nik' => ['required', 'min:10', Rule::unique('voters')->ignore($request->id)],
			'name' => 'required|max:50|min:2',
			'tps' => 'required',
			'identity_photo' => 'image',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
        	if ($request->hasFile('identity_photo')) {
	        	$file = $request->identity_photo;
	        	$extension = $file->getClientOriginalExtension();
	        	$filename  = $request->nik . '.' . $extension;
	            $path = 'id_card/'. $filename;
	            Image::make($file->getRealPath())->widen(800)->save($path);

	            DB::table('voters')
	            ->where('id',$request->id)
	            ->update([
	                'nik' => $request->nik, 
					'name' => $request->name, 
					'address' => $request->address, 
					'gender' => $request->gender,
					'tps_id' => $request->tps,
					'photo' => $filename,
					'updated_at' => Carbon::now()
	            ]);
	        }else{
	        	DB::table('voters')
	            ->where('id',$request->id)
	            ->update([
	                'nik' => $request->nik, 
					'name' => $request->name, 
					'address' => $request->address, 
					'gender' => $request->gender,
					'tps_id' => $request->tps,
					'updated_at' => Carbon::now()
	            ]);
	        }
        	
			Session::flash('message', 'Data telah berhasil diperbarui.');
			return back();	
		}
    }
    public function votersDelete($id){
    	$data = DB::table('voters')->where('id',$id)->first();
    	if(!empty($data->photo)){
    		$path = 'id_card/'.$data->photo;
            File::delete($path);
    	}
    	DB::table('voters')->where('id', $id)->delete();
    	Session::flash('message', 'Data telah berhasil dihapus.');
		return back();
    }
    /*End Voters*/


	/*Begin Users Module*/
	public function users(){
    	$data = DB::table('users')
    	->leftJoin('role_user', 'role_user.user_id', '=', 'id')
    	->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
        ->leftJoin('kelurahan', 'kelurahan.id', '=', 'kelurahan_id')
        ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
    	->select('users.*', 'roles.display_name', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan')
    	->orderBy('users.name', 'asc')
    	->get();
    	return view('backend.limitless.modules.users.index')->with('data', $data);
    }
    public function usersCreate(){
        $kecamatan = DB::table('kecamatan')->orderBy('name', 'asc')->get();
    	$roles = DB::table('roles')->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.users.create')->with('roles', $roles)->with('kecamatan', $kecamatan);
    }
    public function usersCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'email' => 'required|min:5|unique:users,email',
			'name' => 'required|max:50|min:2',
			'password' => 'required|confirmed|min:6',
		]);
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
			$id = DB::table('users')->insertGetId([
				'name' => $request->name, 
				'email' => $request->email, 
                'telpon' => $request->telpon,
                'kelurahan_id' => $request->kelurahan,
				'password' =>  Hash::make($request->password),
				'created_at' => Carbon::now()
    		]);
			DB::table('role_user')->insert([
				'user_id' => $id, 
				'role_id' => $request->role
    		]);
			Session::flash('message', 'Data telah berhasil dibuat.');
			return back();	
		}
    }
    public function usersUpdate($id){
    	$data = DB::table('users')
    		->leftJoin('role_user', 'role_user.user_id', '=', 'id')
            ->leftJoin('kelurahan', 'kelurahan.id', '=', 'kelurahan_id')
            ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
            ->select('users.*', 'kecamatan.id AS kecamatan', 'kelurahan.id AS kelurahan', 'role_user.role_id')
    		->where('users.id', $id)
    		->first();
        $kecamatan = DB::table('kecamatan')->orderBy('name', 'asc')->get();
        $kelurahan = DB::table('kelurahan')->where('kecamatan_id', $data->kecamatan)->orderBy('name', 'asc')->get();
    	$roles = DB::table('roles')->orderBy('display_name', 'asc')->get();
    	return view('backend.limitless.modules.users.update')
            ->with('kecamatan', $kecamatan)
            ->with('kelurahan', $kelurahan)
    		->with('roles', $roles)
    		->with('d', $data);
    }
    public function usersUpdateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'email' => ['required', 'min:5', Rule::unique('users')->ignore($request->id)],
			'name' => 'required|max:50|min:2',
			'password' => 'confirmed|min:6',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
        	if ($request->has('password')) {
	            DB::table('users')
	            ->where('id',$request->id)
	            ->update([
	                'email' => $request->email, 
					'name' => $request->name, 
                    'telpon' => $request->telpon,
                    'kelurahan_id' => $request->kelurahan,
					'password' =>  Hash::make($request->password),
					'updated_at' => Carbon::now()
	            ]);
	            DB::table('role_user')
	            ->where('user_id',$request->id)
	            ->update([
	                'role_id' => $request->role
	            ]);
	        }else{
	        	DB::table('users')
	            ->where('id',$request->id)
	            ->update([
	                'email' => $request->email, 
					'name' => $request->name,
                    'kelurahan_id' => $request->kelurahan,
					'updated_at' => Carbon::now()
	            ]);
	            DB::table('role_user')
	            ->where('user_id',$request->id)
	            ->update([
	                'role_id' => $request->role
	            ]);
	        }
        	
			Session::flash('message', 'Data telah berhasil diperbarui.');
			return back();	
		}
    }
    public function usersDelete($id){
    	DB::table('users')->where('id', $id)->delete();
    	DB::table('role_user')->where('user_id', $id)->delete();
    	Session::flash('message', 'Data telah berhasil dihapus.');
		return back();
    }
	/*End Users*/

	/*Begin Districts Module*/
	public function districts(){
    	$data = DB::table('kecamatan')->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.districts.index')->with('data', $data);
    }
    public function districtsCreate(){
    	return view('backend.limitless.modules.districts.create');
    }
    public function districtsCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'name' => 'required|max:50|min:2',
		]);
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
			DB::table('kecamatan')->insert([
				'name' => $request->name,
				'created_at' => Carbon::now()
    		]);
			Session::flash('message', 'Data telah berhasil dibuat.');
			return back();	
		}
    }
    public function districtsUpdate($id){
    	$data = DB::table('kecamatan')
    		->where('id', $id)
    		->first();
    	return view('backend.limitless.modules.districts.update')
    		->with('d', $data);
    }
    public function districtsUpdateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'name' => 'required|max:50|min:2',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
            DB::table('kecamatan')
            ->where('id',$request->id)
            ->update([
                'name' => $request->name,
				'updated_at' => Carbon::now()
            ]);

			Session::flash('message', 'Data telah berhasil diperbarui.');
			return back();	
		}
    }
    public function districtsDelete($id){
    	DB::table('kecamatan')->where('id', $id)->delete();
    	Session::flash('message', 'Data telah berhasil dihapus.');
		return back();
    }
    /*End Districts*/

    /*Begin Villages Module*/
	public function villages($id){
    	$data = DB::table('kelurahan')
    		->leftJoin('kecamatan', 'kecamatan.id', '=', 'kecamatan_id')
    		->select('kelurahan.*', 'kecamatan.name AS kecamatan')
    		->where('kecamatan_id', $id)
    		->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.villages.index')->with('data', $data)->with('id', $id);
    }
    public function villagesCreate($id){
    	return view('backend.limitless.modules.villages.create')->with('id', $id);
    }
    public function villagesCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'name' => 'required|max:50|min:2',
			'kecamatan_id' => 'required|exists:kecamatan,id',
		]);
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
			DB::table('kelurahan')->insert([
				'name' => $request->name,
				'kecamatan_id' => $request->kecamatan_id,
				'created_at' => Carbon::now()
    		]);
			Session::flash('message', 'Data telah berhasil dibuat.');
			return back();	
		}
    }
    public function villagesUpdate($kecamatan, $id){
    	$data = DB::table('kelurahan')
    		->where('id', $id)
    		->first();
    	return view('backend.limitless.modules.villages.update')
    		->with('d', $data)->with('kecamatan', $kecamatan);
    }
    public function villagesUpdateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'name' => 'required|max:50|min:2',
			'kecamatan_id' => 'required|exists:kecamatan,id',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
            DB::table('kelurahan')
            ->where('id',$request->id)
            ->update([
                'name' => $request->name,
				'updated_at' => Carbon::now()
            ]);

			Session::flash('message', 'Data telah berhasil diperbarui.');
			return back()->with('id', $request->id)->with('kecamatan', $request->kecamatan_id);	
		}
    }
    public function villagesDelete($id){
    	DB::table('kelurahan')->where('id', $id)->delete();
    	Session::flash('message', 'Data telah berhasil dihapus.');
		return back();
    }
    /*End Villages*/

    /*Begin tps Module*/
	public function tps($kecamatan, $kelurahan){
    	$data = DB::table('tps')
    		->leftJoin('kelurahan', 'kelurahan.id', '=', 'kelurahan_id')
    		->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
    		->select('tps.*', 'kelurahan.name AS kelurahan', 'kelurahan.kecamatan_id', 'kecamatan.name AS kecamatan')
    		->where('kelurahan_id', $kelurahan)
    		->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.tps.index')->with('data', $data)->with('kecamatan', $kecamatan)->with('kelurahan', $kelurahan);
    }
    public function tpsCreate($kecamatan, $kelurahan){
    	return view('backend.limitless.modules.tps.create')->with('kecamatan', $kecamatan)->with('kelurahan', $kelurahan);
    }
    public function tpsCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'name' => 'required',
			'kelurahan_id' => 'required|exists:kelurahan,id',
		]);
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
			DB::table('tps')->insert([
				'name' => $request->name,
				'kelurahan_id' => $request->kelurahan_id,
				'created_at' => Carbon::now()
    		]);
			Session::flash('message', 'Data telah berhasil dibuat.');
			return back();	
		}
    }
    public function tpsUpdate($kecamatan, $kelurahan, $id){
    	$data = DB::table('tps')
    		->where('id', $id)
    		->first();
    	return view('backend.limitless.modules.tps.update')
    		->with('d', $data)->with('kecamatan', $kecamatan)->with('kelurahan', $kelurahan)->with('id', $id);
    }
    public function tpsUpdateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'name' => 'required',
			'kelurahan_id' => 'required|exists:kelurahan,id',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
            DB::table('tps')
            ->where('id',$request->id)
            ->update([
                'name' => $request->name,
				'updated_at' => Carbon::now()
            ]);

			Session::flash('message', 'Data telah berhasil diperbarui.');
			return back()->with('id', $request->id)->with('kelurahan', $request->kelurahan_id);	
		}
    }
    public function tpsDelete($id){
    	DB::table('tps')->where('id', $id)->delete();
    	Session::flash('message', 'Data telah berhasil dihapus.');
		return back();
    }
    /*End tps*/

    /*Begin result Module*/
    public function result(){
        if(Entrust::hasRole('admin')){
            $data = DB::table('result')
                ->leftJoin('tps', 'tps.id', '=', 'tps_id')
                ->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
                ->leftJoin('users', 'users.id', '=', 'created_by')
                ->select('result.*', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan', 'users.name AS creator')
                ->latest()
                ->get();
        }
        elseif (Entrust::hasRole('coordinator')) {
            $data = DB::table('result')
                ->leftJoin('tps', 'tps.id', '=', 'tps_id')
                ->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
                ->leftJoin('users', 'users.id', '=', 'created_by')
                ->select('result.*', 'kelurahan.name AS kelurahan', 'kecamatan.name AS kecamatan', 'users.name AS creator')
                ->where('created_by', Auth::id())
                ->latest()
                ->get();
        }
    	return view('backend.limitless.modules.result.index')->with('data', $data);
    }
    public function resultCreate(){
    	$kecamatan = DB::table('kecamatan')->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.result.create')->with('kecamatan', $kecamatan);
    }
    public function resultCreateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'amount' => 'required|integer',
			'tps' => 'required',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
			DB::table('result')->insert([
				'tps_id' => $request->tps, 
				'amount' => $request->amount, 
				'created_by' => Auth::id(),
				'created_at' => Carbon::now()
    		]);
			Session::flash('message', 'Data telah berhasil dibuat.');
			return back();	
		}
    }
    public function resultUpdate($id){
    	$data = DB::table('result')
    		->leftJoin('tps', 'tps.id', '=', 'tps_id')
    		->leftJoin('kelurahan', 'kelurahan.id', '=', 'tps.kelurahan_id')
    		->leftJoin('kecamatan', 'kecamatan.id', '=', 'kelurahan.kecamatan_id')
    		->select('result.*', 'kelurahan.id AS kelurahan', 'kecamatan.id AS kecamatan')
    		->where('result.id', $id)
    		->first();
    	$kecamatan = DB::table('kecamatan')->orderBy('name', 'asc')->get();
    	$kelurahan = DB::table('kelurahan')->where('kecamatan_id', $data->kecamatan)->orderBy('name', 'asc')->get();
    	$tps = DB::table('tps')->where('kelurahan_id', $data->kelurahan)->orderBy('name', 'asc')->get();
    	return view('backend.limitless.modules.result.update')
    		->with('kecamatan', $kecamatan)
    		->with('kelurahan', $kelurahan)
    		->with('tps', $tps)
    		->with('d', $data);
    }
    public function resultUpdateSubmit(Request $request){
        $validator = Validator::make($request->all(), [
			'amount' => 'required|integer',
			'tps' => 'required',
		]);
		
		if ($validator->fails()) {
            return back()
            	->withErrors($validator)
            	->withInput();
        }
        else{
            DB::table('result')
            ->where('id',$request->id)
            ->update([
				'tps_id' => $request->tps,
				'amount' => $request->amount,
				'updated_at' => Carbon::now()
            ]);
        	
			Session::flash('message', 'Data telah berhasil diperbarui.');
			return back();	
		}
    }
    public function resultDelete($id){
    	DB::table('result')->where('id', $id)->delete();
    	Session::flash('message', 'Data telah berhasil dihapus.');
		return back();
    }
    /*End result*/
}
