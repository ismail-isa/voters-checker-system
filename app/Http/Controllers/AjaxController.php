<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AjaxController extends Controller
{
    public function kelurahan($id){
        $kelurahan = DB::table('kelurahan')->where('kecamatan_id', $id)->orderBy('name', 'asc')->pluck('name', 'id');
        return json_encode($kelurahan);
    }

    public function tps($id){
        $tps = DB::table('tps')->where('kelurahan_id', $id)->orderBy('name', 'asc')->pluck('name', 'id');
        return json_encode($tps);
    }
}
